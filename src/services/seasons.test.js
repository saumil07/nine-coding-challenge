import {getSeasons} from "./seasons";

const EPISODE = [
    {
        drm: false,
        episodeCount: 5,
        image: {
            showImage: "test1.jpg"
        },
        slug: "test1",
        title: "title1"
    },
    {
        drm: true,
        episodeCount: 0,
        image: {
            showImage: "test2.jpg"
        },
        slug: "test2",
        title: "title2"
    }
];



describe('Seasons Service', () => {

    it('should return empty array if payload is empty', () => {
        const payload = [];
        const respose = getSeasons(payload)
        expect(respose).toEqual([]);
    });

    it('should return empty array if drn false or episodeCount = 0 ', () => {
        const respose = getSeasons(EPISODE);
        expect(respose).toEqual([]);
    });

    it('should return episode array', () => {
        const payload = EPISODE.map(e => ({ ...e, drm: true,episodeCount : 5 }));
        const respose = getSeasons(payload);
        expect(respose).toEqual([
            {
                "image": "test1.jpg",
                "slug": "test1",
                "title": "title1",
            },
            {
                "image": "test2.jpg",
                "slug": "test2",
                "title": "title2",
            }
        ]);
    });
});