import get from "lodash/get";

export const getSeasons = (payload) => (
    payload.reduce((acc, season) => {
        if (season.drm === true && season.episodeCount > 0) {
            acc.push({
                image: get(season, 'image.showImage', ''),
                slug: season.slug,
                title: season.title

            })
        }
        return acc;
    }, [])
);