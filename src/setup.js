import bodyParser from "body-parser";
import routes from "./routes";

const setup = app => {

    app.use(bodyParser.json(), bodyParser.urlencoded({
        extended: true
    }));

    app.use((err, req, res, next) => {
        if (err instanceof SyntaxError && err.status === 400 && "body" in err) {
            return res.status(400).json({
                error: "Could not decode request: JSON parsing failed"
            });
        } else {
            next();
        }

    });

    routes(app);

};

export default setup;