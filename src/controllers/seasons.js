import {getSeasons} from "../services/seasons";


export const seasons = (req,res,next) => {
    try {
        return res.json({
            response: getSeasons(req.body.payload)
        });
    } catch (e) {
        return next(e);
    }
};