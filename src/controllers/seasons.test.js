import * as service from '../services/seasons'
import {seasons} from "./seasons";


describe('Seasons Controller', () => {
    const req = {
        body: {
            payload: []
        }
    };

    const res = {
        json: jest.fn()
    };

    const next = jest.fn();

    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('should call seasons service method with payload', () => {
        const spyon = jest.spyOn(service, 'getSeasons');
        seasons(req, res, next)
        expect(spyon).toHaveBeenCalledWith([])
        expect(next).toBeCalledTimes(0);
    });

    it('should call next on error', () => {
        jest.spyOn(service, 'getSeasons').mockReturnValue(1);
        seasons([], res, next)
        expect(next).toBeCalledTimes(1);
    });
});