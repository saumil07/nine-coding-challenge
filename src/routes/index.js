import SeasonsRoute from "./seasons";

export default app => {
    app.use("/", SeasonsRoute);


    app.use("/*", (req, res) => {
        res.status(404).json({
            error: "Not Found"
        });
    });
};