import request from 'supertest';
import app from '../app';

describe('Seasons Route ', () => {
    describe('POST', () => {
        it('should return status code 200 if valid json pass ', async () => {

            const result = await request(app)
                .post('/')
                .send({
                    payload :[]
                });
            expect(result.statusCode).toEqual(200);
        });

        it('should return status code 400 if json invalid', async () => {

            const result = await request(app)
                .post('/')
                .send({
                    result :[]
                });
            expect(result.statusCode).toEqual(400);
        });

        it('should return status code 400 if request body invalid', async () => {

            const result = await request(app)
                .post('/')
                .send("sdsdfsdf");
            expect(result.statusCode).toEqual(400);
        });

        it('should return status code 404 if route not found', async () => {

            const result = await request(app)
                .get('/');
            expect(result.statusCode).toEqual(404);
        });
    });

});