import express from "express";
import  {seasons} from "../controllers/seasons";
import {validateSchema} from '../middlewares/validator';

const router = express.Router();

router.post("/", validateSchema('episode'), seasons);

export default router;