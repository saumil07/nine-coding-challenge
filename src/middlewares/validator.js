import Ajv from 'ajv';
import episodeSchema from '../schema/episode'

let ajv = Ajv({allErrors: true});
ajv.addSchema(episodeSchema, 'episode');

export const validateSchema = (schemaName) => ((req, res, next) => {
    try {
        let valid = ajv.validate(schemaName, req.body);

        if (!valid) {
            return res.status(400).json({
                error: "Could not decode request: JSON parsing failed"
            });
        }

        next();
    } catch (e) {
        next();
    }

});