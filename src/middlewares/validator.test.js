import {validateSchema} from "./validator";

describe('Validator Middleware', () => {
    const req = {
        body : {
            payload : []
        }
    };

    const res = {
        status: jest.fn().mockImplementation(() => res),
        json: jest.fn().mockImplementation(() => res)
    };

    const next = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should validate episode schema and call next ', () =>  {
        validateSchema('episode')(req,res,next);
        expect(next).toHaveBeenCalledTimes(1);
    });

    it('should return error if schme valiadtion fails', () => {
        const req = {
            body : {}
        };
        validateSchema('episode')(req,res,next);
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            error: "Could not decode request: JSON parsing failed"
        });
        expect(next).toHaveBeenCalledTimes(0);
    });

    it('should validate  ', () => {
        validateSchema('episode-new')(req,res,next);
        expect(next).toHaveBeenCalledTimes(1);
    });



});