# Nine Digital Coding Challenge

## Run Application

```sh
npm install
npm run build
npm run start
```

## Run Tests

```sh
npm run test
```